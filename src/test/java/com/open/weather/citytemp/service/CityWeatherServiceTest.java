package com.open.weather.citytemp.service;

import com.open.weather.citytemp.model.City;
import com.open.weather.citytemp.repository.CityWeatherRepository;
import com.open.weather.citytemp.response.CityAvgTempDto;
import com.open.weather.citytemp.response.CityDto;
import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class CityWeatherServiceTest {

    @Autowired
    private CityWeatherService cityWeatherService;

    @MockBean
    private CityWeatherRepository repository;

    @Test
    @DisplayName("Find average temperatures for available cities")
    void findAvgCitiesTemp() {
        List<CityAvgTempDto> cities = new ArrayList<>();
        List<Long> cityIds = new ArrayList<>();
        Mockito.doReturn(cities).when(repository).findCitiesAvgTemp(cityIds);

        List<CityAvgTempDto> avgCities = cityWeatherService.findAvgCitiesTemp();

        Assertions.assertTrue(avgCities.isEmpty());
        Assertions.assertFalse(!avgCities.isEmpty());
    }

    @Test
    @DisplayName("Find available cities")
    void getAvailableCities() {
        List<com.open.weather.citytemp.model.City> cities = new ArrayList<>();
        City city1 = new City(2643743l, "London");
        City city2 = new City(524901l, "Moscow");
        City city3 = new City(2988507l, "Paris");

        cities.add(city1);
        cities.add(city2);
        cities.add(city3);

        Mockito.doReturn(cities).when(repository).findAllCities();

        List<CityDto> listDto = IterableUtils.toList(cityWeatherService.findAllCities());
        Assertions.assertFalse(listDto.isEmpty());
        Assertions.assertTrue(listDto.size() == 3);

        for (int i = 0; i < 3; i++) {
            Assertions.assertTrue(cities.get(i).getCityName().equals(listDto.get(i).getCityName()));
            Assertions.assertTrue(cities.get(i).getId().equals(listDto.get(i).getCityId()));
        }
    }
}