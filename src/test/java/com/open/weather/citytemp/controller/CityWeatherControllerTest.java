package com.open.weather.citytemp.controller;

import com.open.weather.citytemp.response.CityAvgTempDto;
import com.open.weather.citytemp.response.CityDto;
import com.open.weather.citytemp.service.CityWeatherService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class CityWeatherControllerTest {

    @MockBean
    private CityWeatherService cityWeatherService;

    @Autowired
    private MockMvc mockMcv;

    @Test
    void availableCities() throws Exception {
        CityDto dot1 = new CityDto(2643743L, "London", new ArrayList<>());
        CityDto dot2 = new CityDto(524901L, "Moscow", new ArrayList<>());
        CityDto dot3 = new CityDto(2988507L, "Paris", new ArrayList<>());

        List<CityDto> cities = new ArrayList<>();
        cities.add(dot1);
        cities.add(dot2);
        cities.add(dot3);


        Mockito.doReturn(cities).when(cityWeatherService).findAllCities();

        mockMcv.perform(get("/weather/cities"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].cityId", is(dot1.getCityId().intValue())))
                .andExpect(jsonPath("$[0].cityName", is(dot1.getCityName())))
                .andExpect(jsonPath("$[1].cityId", is(dot2.getCityId().intValue())))
                .andExpect(jsonPath("$[1].cityName", is(dot2.getCityName())))
                .andExpect(jsonPath("$[2].cityId", is(dot3.getCityId().intValue())))
                .andExpect(jsonPath("$[2].cityName", is(dot3.getCityName())));
    }

    @Test
    void averageTempOfCities() throws Exception {
        CityAvgTempDto dto1 = new CityAvgTempDto("London", BigDecimal.TEN);
        CityAvgTempDto dto2 = new CityAvgTempDto("Moscow", BigDecimal.TEN);
        CityAvgTempDto dto3 = new CityAvgTempDto("Paris", BigDecimal.TEN);

        List<CityAvgTempDto> averageTempCityDtos = new ArrayList<>();
        averageTempCityDtos.add(dto1);
        averageTempCityDtos.add(dto2);
        averageTempCityDtos.add(dto3);

        Mockito.doReturn(averageTempCityDtos).when(cityWeatherService).findAvgCitiesTemp();

        mockMcv.perform(get("/weather/cities/average-temperature"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].cityName", is(dto1.getCityName())))
                .andExpect(jsonPath("$[0].tempAvg", is(dto1.getTempAvg().intValue())))
                .andExpect(jsonPath("$[1].cityName", is(dto2.getCityName())))
                .andExpect(jsonPath("$[1].tempAvg", is(dto2.getTempAvg().intValue())))
                .andExpect(jsonPath("$[2].cityName", is(dto3.getCityName())))
                .andExpect(jsonPath("$[2].tempAvg", is(dto3.getTempAvg().intValue())));

    }


}