package com.open.weather.citytemp.repository;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.github.database.rider.junit5.DBUnitExtension;
import com.open.weather.citytemp.mappers.OpenWeatherMapper;
import com.open.weather.citytemp.model.City;
import com.open.weather.citytemp.model.Weather;
import com.open.weather.citytemp.response.CityAvgTempDto;
import com.open.weather.citytemp.response.weatherapi.WeatherMapDto;
import org.apache.commons.collections.ArrayStack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(DBUnitExtension.class)
@SpringBootTest
@ActiveProfiles("test")
class CityWeatherRepositoryTest {

    private final static String RESOURCE_PATH = "src/test/resources/test-file/open_weather_api_response.json";
    @Autowired
    private CityWeatherRepository repository;

    private static JsonMapper mapper;

    private List<Weather> weathers = new ArrayStack();


    @BeforeAll
    public static void init() {
        mapper = new JsonMapper();
        mapper.configure(DeserializationFeature.EAGER_DESERIALIZER_FETCH.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Test
    @DisplayName("Weather batch insert and find all by City")
    void weatherBatchInsertAndFindAllByCityId() throws IOException {
        File jsonFile = new File(RESOURCE_PATH);
        if (jsonFile.exists()) {
            final WeatherMapDto responseDto = mapper.readValue(jsonFile, WeatherMapDto.class);
            final City city = Mappers.getMapper(OpenWeatherMapper.class).toCity(responseDto);

            // batch insert
            for (int r : repository.saveWeathers(city.getWeathers()))
                Assertions.assertTrue(r == BigDecimal.ONE.intValue());


            List<Weather> weathers = repository.findAllWeathersByCityId(city.getId());
            Assertions.assertFalse(weathers.isEmpty());
            Assertions.assertTrue(weathers.size() >= city.getWeathers().size());
        } else
            Assertions.fail();
    }

    @Test
    @DisplayName("Find average temperatures for available cities")
    void findAvgTempForCities() {
        List<Long> cities = new ArrayList<>();
        cities.add(2643743L);
        cities.add(524901L);
        cities.add(2988507L);

        List<CityAvgTempDto> avgTemps = repository.findCitiesAvgTemp(cities);
        Assertions.assertFalse(avgTemps.isEmpty());
        Assertions.assertTrue(avgTemps.size() == 3);


        // comparing average temperature with zero, because weather data are changed
        avgTemps.forEach(avt ->
                Assertions.assertTrue(avt.getTempAvg().compareTo(BigDecimal.ZERO) == BigDecimal.ONE.intValue()));
    }

    @Test
    void findAllCities() {
        //TODO: after implementing oneToMany with JOIN
    }

    @Test
    @DisplayName("Find available cities")
    void findAvailableCities() {
        List<City> cities = repository.findAllCitiesIdAndName();
        Assertions.assertFalse(cities.isEmpty());
        Assertions.assertTrue(cities.size() == 3);
    }
}