package com.open.weather.citytemp.dao;

import com.open.weather.citytemp.model.City;
import com.open.weather.citytemp.model.Weather;
import com.open.weather.citytemp.repository.CityWeatherRepository;
import com.open.weather.citytemp.response.CityAvgTempDto;
import com.open.weather.citytemp.util.QueryHelper;
import org.simpleflatmapper.jdbc.spring.JdbcTemplateMapperFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class CityWeatherDao implements CityWeatherRepository {

    private final JdbcTemplate jdbc;

    @Autowired
    public CityWeatherDao(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Transactional
    @Override
    public int[] saveWeathers(List<Weather> weathers) {
        return jdbc.batchUpdate("INSERT into WEATHER (id, city_id, temp_value, main, dt, created_at) VALUES "
                + "(?,?,?,?,?,?)", new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setInt(1, weathers.get(i).getId().intValue());
                ps.setInt(2, weathers.get(i).getCityId().intValue());
                ps.setBigDecimal(3, weathers.get(i).getTempValue());
                ps.setString(4, weathers.get(i).getMain());
                ps.setString(5, weathers.get(i).getDt());
                ps.setDate(6, new Date(System.currentTimeMillis()));
            }

            @Override
            public int getBatchSize() {
                return weathers.size();
            }
        });
    }

    @Override
    public List<Weather> findAllWeathersByCityId(Long cityId) {
        return jdbc.query("SELECT ind, id, city_id, temp_value, main, dt " +
                        "FROM Weather WHERE city_id = ?",
                (rs, rowNum) ->
                        Weather.builder()
                                .ind(rs.getInt("ind"))
                                .id(rs.getLong("id"))
                                .cityId(rs.getLong("city_id"))
                                .tempValue(rs.getBigDecimal("temp_value"))
                                .main(rs.getString("main"))
                                .dt(rs.getString("dt"))
                                .build(), cityId);
    }

    @Override
    public List<CityAvgTempDto> findCitiesAvgTemp(List<Long> cities) {

        final String inSql = cities.stream().map(String::valueOf)
                .collect(Collectors.joining(","));

        return jdbc.query(
                QueryHelper.CITIES_AVG_TEMP,
                (rs, rowNum) -> new CityAvgTempDto(
                        rs.getString("city_name"),
                        rs.getBigDecimal("avg_temp")));
    }

    @Override
    public Iterable<City> findAllCities() {
        return jdbc.query(
                QueryHelper.FIND_ALL_CITIES,
                JdbcTemplateMapperFactory
                        .newInstance()
                        .addKeys("id", "weathers_ind") // the  id column for City
                        .newResultSetExtractor(City.class));
    }

    @Override
    public List<City> findAllCitiesIdAndName() {
        return jdbc.query("SELECT id, city_name " +
                        "FROM city",
                (rs, rowNum) -> new City(
                        rs.getLong("id"),
                        rs.getString("city_name")));
    }

    @Override
    public Optional<City> findCityById(Long id) {
        final List<City> cities = jdbc.query(
                String.format(QueryHelper.FIND_CITY_BY_ID, id),
                JdbcTemplateMapperFactory
                        .newInstance()
                        .addKeys("id", "weathers_ind") // the  id column for City
                        .newResultSetExtractor(City.class));

        // TODO: discuss about this part of code
        return cities.size() == BigDecimal.ONE.intValue() ? Optional.ofNullable(cities.get(BigDecimal.ZERO.intValue()))
                : Optional.empty();
    }

    @Override
    public Optional<CityAvgTempDto> findCityAvgTemp(Long id) {
        return Optional.ofNullable(jdbc.queryForObject(String.format(QueryHelper.FIND_CITY_AVG_TEMP, id),
                (rs, rowNum) -> new CityAvgTempDto(rs.getString("city_name"),
                        rs.getBigDecimal("avg_temp"))));
    }
}
