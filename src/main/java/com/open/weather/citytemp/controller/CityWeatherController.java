package com.open.weather.citytemp.controller;

import com.google.common.base.Preconditions;
import com.open.weather.citytemp.response.CityAvgTempDto;
import com.open.weather.citytemp.response.CityDto;
import com.open.weather.citytemp.service.CityWeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/weather")
public class CityWeatherController {

    protected CityWeatherService cityWeatherService;

    @Autowired
    public CityWeatherController(CityWeatherService cityWeatherService) {
        this.cityWeatherService = cityWeatherService;
    }

    @GetMapping("/cities")
    public ResponseEntity<List<CityDto>> findAllCities() {
        return new ResponseEntity(cityWeatherService.findAllCities(), HttpStatus.OK);
    }

    @GetMapping("/cities/average-temperature")
    public ResponseEntity<List<CityAvgTempDto>> averageTempOfCities() {
        return new ResponseEntity(cityWeatherService.findAvgCitiesTemp(), HttpStatus.OK);
    }

    @GetMapping("/city/{id}")
    public ResponseEntity<CityDto> getCityById(@PathVariable long id) {
        Preconditions.checkArgument(id > BigDecimal.ZERO.longValue());
        return ResponseEntity.of(cityWeatherService.getCityById(id));
    }

    @GetMapping("/city/{id}/average-temperature")
    public ResponseEntity<CityAvgTempDto> getCityAvgTemp(@PathVariable long id) {
        Preconditions.checkArgument(id > BigDecimal.ZERO.longValue());
        return ResponseEntity.of(cityWeatherService.getCityAvgTemp(id));
    }
}
