package com.open.weather.citytemp.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CityAvgTempDto {
    private String cityName;
    private BigDecimal tempAvg;
}
