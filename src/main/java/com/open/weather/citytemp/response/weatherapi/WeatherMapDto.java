package com.open.weather.citytemp.response.weatherapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeatherMapDto {

    private String cod;
    private String message;
    private Integer cnt;
    private List<WeatherMapListDto> list = new ArrayList<>();
    private WeatherMapCityDto city;

}
