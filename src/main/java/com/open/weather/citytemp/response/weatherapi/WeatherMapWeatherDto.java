package com.open.weather.citytemp.response.weatherapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeatherMapWeatherDto {
    private Long id;
    private String main;
}
