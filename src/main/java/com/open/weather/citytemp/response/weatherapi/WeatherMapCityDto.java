package com.open.weather.citytemp.response.weatherapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeatherMapCityDto {
    private Long id;
    private String name;
    private String country;
    private Long timezone;
}
