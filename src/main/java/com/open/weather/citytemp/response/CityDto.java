package com.open.weather.citytemp.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityDto {
    Long cityId;
    String cityName;
    List<WeatherDto> weathers;
}