package com.open.weather.citytemp.response.weatherapi;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeatherMapListDto {
    private Timestamp dt;
    private WeatherMapMainDto main;
    private List<WeatherMapWeatherDto> weather;
    @JsonProperty("dt_txt")
    private String dtTxt;
}
