package com.open.weather.citytemp.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeatherDto {
    private Long id;
    private Double tempValue;
    private String main;
    private String dt;
}
