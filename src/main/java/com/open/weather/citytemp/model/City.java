package com.open.weather.citytemp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class City {

    private Long id;
    private String cityName;
    private String country;
    private Integer timezone;
    private List<Weather> weathers;

    public City(Long id, String cityName, String country, Integer timezone) {
        this.id = id;
        this.cityName = cityName;
        this.country = country;
        this.timezone = timezone;
    }

    public City(Long id, String cityName) {
        this.id = id;
        this.cityName = cityName;
    }
}
