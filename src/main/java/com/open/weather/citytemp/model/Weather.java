package com.open.weather.citytemp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Weather {
    private Integer ind;
    private Long id;
    private Long cityId;
    private BigDecimal tempValue;
    private String main;
    private String dt;
}
