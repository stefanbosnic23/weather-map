package com.open.weather.citytemp.mappers;

import com.open.weather.citytemp.model.City;
import com.open.weather.citytemp.model.Weather;
import com.open.weather.citytemp.response.weatherapi.WeatherMapDto;
import com.open.weather.citytemp.response.weatherapi.WeatherMapListDto;
import org.mapstruct.*;



@Mapper(componentModel = "spring")
public interface OpenWeatherMapper {

    @AfterMapping
    default void fillUp(@MappingTarget City target) {
        target.getWeathers().forEach(weather -> weather.setCityId(target.getId()));
    }

    @Mappings({
            @Mapping(target = "id", source = "city.id"),
            @Mapping(target = "cityName", source = "city.name"),
            @Mapping(target = "country", source = "city.country"),
            @Mapping(target = "timezone", source = "city.timezone"),
            @Mapping(target = "weathers", source = "list")
    })
    City toCity(WeatherMapDto dto);


    @Mappings({
            @Mapping(target = "tempValue", source = "main.temp"),
            @Mapping(target = "dt", source = "dtTxt"),
            @Mapping(expression = "java(dto.getWeather().get(0).getMain())", target = "main"),
            @Mapping(expression = "java(dto.getWeather().get(0).getId())", target = "id"),
            @Mapping(target = "cityId", ignore = true),
            @Mapping(target = "ind", ignore = true)
    })
    Weather toWeather(WeatherMapListDto dto);
}
