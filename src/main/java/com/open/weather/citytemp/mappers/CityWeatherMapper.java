package com.open.weather.citytemp.mappers;

import com.open.weather.citytemp.model.City;
import com.open.weather.citytemp.model.Weather;
import com.open.weather.citytemp.response.CityDto;
import com.open.weather.citytemp.response.WeatherDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;


@Mapper(componentModel = "spring")
public interface CityWeatherMapper {

    @Mapping(target = "cityId", source = "id")
    CityDto toCityDto(City model);

    List<CityDto> toCityDtoList(Iterable<City> models);

    WeatherDto toWeatherDto(Weather weather);

    List<WeatherDto> toWeatherDtoList(List<Weather> weathers);


}
