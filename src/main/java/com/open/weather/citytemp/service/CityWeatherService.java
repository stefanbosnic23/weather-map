package com.open.weather.citytemp.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.open.weather.citytemp.mappers.CityWeatherMapper;
import com.open.weather.citytemp.mappers.OpenWeatherMapper;
import com.open.weather.citytemp.repository.CityWeatherRepository;
import com.open.weather.citytemp.response.CityAvgTempDto;
import com.open.weather.citytemp.response.CityDto;
import com.open.weather.citytemp.response.weatherapi.WeatherMapDto;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
@Log4j2
public class CityWeatherService implements InitializingBean {

    protected final static JsonMapper mapper = new JsonMapper();

    @Value("${URL_PATH}")
    private String URL_PATH;
    @Value("${API_KEY}")
    private String API_KEY;
    @Value("${URI_PATH}")
    private String URI_PATH;

    public CityWeatherRepository cityWeatherRepo;

    @Autowired
    public CityWeatherService(CityWeatherRepository cityWeatherRepo) {
        this.cityWeatherRepo = cityWeatherRepo;
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public WeatherMapDto performApiByCityName(String cityName) throws JsonProcessingException {
        return mapper.readValue(WebClient.create(URL_PATH)
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(URI_PATH)
                        .queryParam("q", cityName)
                        .queryParam("appid", API_KEY)
                        .build())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .bodyToMono(String.class)
                .block(), WeatherMapDto.class);
    }

    public void fetchAndPersistWeathersByCity() {
        log.info("Open weather map API by city name triggering...");

        cityWeatherRepo.findAllCitiesIdAndName().forEach(c -> {
            try {
                final WeatherMapDto mapDto = performApiByCityName(c.getCityName());
                final com.open.weather.citytemp.model.City city = Mappers.getMapper(OpenWeatherMapper.class).toCity(mapDto);
                int[] batch = cityWeatherRepo.saveWeathers(city.getWeathers());
                log.info("Size of batch: " + batch.length);

            } catch (JsonProcessingException e) {
                log.error("JSON deserialization failed! ", e);
            }
        });

        log.info("Open weather map API by city name completed!");
    }

    public List<CityAvgTempDto> findAvgCitiesTemp() {
        final List<Long> cityIDs = new ArrayList<>();
        cityWeatherRepo.findAllCitiesIdAndName().forEach(c -> cityIDs.add(c.getId()));

        return cityWeatherRepo.findCitiesAvgTemp(cityIDs);
    }

    public List<CityDto> findAllCities() {
        return Mappers.getMapper(CityWeatherMapper.class)
                .toCityDtoList(cityWeatherRepo.findAllCities());
    }

    public Optional<CityDto> getCityById(Long id) {
        return Optional.ofNullable(Mappers.getMapper(CityWeatherMapper.class)
                .toCityDto(cityWeatherRepo.findCityById(id).get()));
    }

    public Optional<CityAvgTempDto> getCityAvgTemp(Long id) {
        return cityWeatherRepo.findCityAvgTemp(id);
    }

    @Override
    public void afterPropertiesSet() {
        fetchAndPersistWeathersByCity();
    }
}
