package com.open.weather.citytemp.repository;

import com.open.weather.citytemp.model.City;
import com.open.weather.citytemp.model.Weather;
import com.open.weather.citytemp.response.CityAvgTempDto;

import java.util.List;
import java.util.Optional;

public interface CityWeatherRepository {

    List<Weather> findAllWeathersByCityId(Long cityId);

    int[] saveWeathers(List<Weather> weathers);

    List<CityAvgTempDto> findCitiesAvgTemp(List<Long> cities);

    Iterable<City> findAllCities();

    Optional<City> findCityById(Long id);

    Optional<CityAvgTempDto> findCityAvgTemp(Long id);

    List<City> findAllCitiesIdAndName();
}
