package com.open.weather.citytemp;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "City weather API",
        version = "1.0", description = "City weathers information"))
public class CitytempApplication {

    public static void main(String[] args) {
        SpringApplication.run(CitytempApplication.class, args);
    }

}
