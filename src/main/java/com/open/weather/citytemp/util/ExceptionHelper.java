package com.open.weather.citytemp.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.security.InvalidParameterException;
import java.util.NoSuchElementException;

@RestControllerAdvice
public class ExceptionHelper {

    @ExceptionHandler(value = {InvalidParameterException.class})
    public ResponseEntity<Object> handleInvalidInputException(InvalidParameterException ex) {
        return new ResponseEntity<>(StringUtils.join("Input param is not valid. Message: ", ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {NoSuchElementException.class})
    public ResponseEntity<Object> handleNoSuchElementException(NoSuchElementException ex) {
        return new ResponseEntity<>(StringUtils.join("Element not found. Message: ", ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex) {
        return new ResponseEntity<>("Input param is not valid!", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleException(Exception ex) {
        return new ResponseEntity<>(StringUtils.join("Error message: ", ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {MissingPathVariableException.class})
    public ResponseEntity<Object> handleMissingPathVariableException(MissingPathVariableException ex) {
        return new ResponseEntity<>(StringUtils.join("Invalid path param. Message: ", ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
